package mintowerrange

import (
	"math"
)

const (
	Min int = 0
	Max int = 1000
)

func GetMinTowerRange(listeners, towers []int) int {
	maxMin := 0
	for _, l := range listeners {
		lmin := getMinRangeForListener(l, towers)
		if lmin > maxMin {
			maxMin = lmin
		}
	}
	return maxMin
}

func getMinRangeForListener(listener int, towers []int) int {
	listenerMinRange := Max
	for _, t := range towers {
		min := int(math.Abs(float64(listener - t)))
		if min < listenerMinRange {
			listenerMinRange = min
		}
	}
	return listenerMinRange
}
