package mintowerrange

import (
	"testing"
)

func TestGetMinRangeForListener(t *testing.T) {
	t.Run("returns min difference of number and array", func(t *testing.T) {
		listener := 20
		towers := []int{4, 8, 15}
		got := getMinRangeForListener(listener, towers)
		want := 5
		if got != want {
			t.Errorf("got %d want %d", got, want)
		}

	})

	t.Run("returns min difference of number and array", func(t *testing.T) {
		listener := 5
		towers := []int{4, 8, 15}
		got := getMinRangeForListener(listener, towers)
		want := 1
		if got != want {
			t.Errorf("got %d want %d", got, want)
		}
	})
}

func TestGetMinTowerRange(t *testing.T) {
	t.Run("returns max min difference of 2 arrays", func(t *testing.T) {
		listeners := []int{1, 5, 11, 20}
		towers := []int{4, 8, 15}
		got := GetMinTowerRange(listeners, towers)
		want := 5
		if got != want {
			t.Errorf("got %d want %d", got, want)
		}
	})

	t.Run("returns min difference of number and array", func(t *testing.T) {
		listeners := []int{3, 9, 25, 37}
		towers := []int{7, 18, 23, 29}
		got := GetMinTowerRange(listeners, towers)
		want := 8
		if got != want {
			t.Errorf("got %d want %d", got, want)
		}
	})
}
