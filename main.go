package main

import (
	"fmt"
	mintowerrange "practice/MinTowerRange"
)

func main() {
	listeners := []int{1, 5, 11, 20}
	towers := []int{4, 8, 15}
	minTowerRange := mintowerrange.GetMinTowerRange(listeners, towers)
	fmt.Printf("Minimum broadcast range is %d", minTowerRange)

}
